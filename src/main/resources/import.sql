
INSERT INTO authors (id, name, gender, country_origin, birthdate, date_death) VALUES ('d1000b02-286c-4bb5-a7ec-f5d3478e8215', 'MIGUEL DE CERVANTES','M', 'Espanha', '1547-09-29', '1616-04-22');
INSERT INTO authors (id, name, gender, country_origin, birthdate, date_death) VALUES ('7d88581b-5602-4bf9-9bfd-4bcf11bd1e9a', 'JOANNE ROWLING', 'F','Reino Unido', '1965-07-31', '');
INSERT INTO authors (id, name, gender, country_origin, birthdate, date_death) VALUES ('8db69427-7a1e-40e9-8de5-e4b437a0b6b1', 'GABRIEL GARCIA MARQUEZ', 'M','Colombia', '1927-04-06', '2014-04-17');
INSERT INTO authors (id, name, gender, country_origin, birthdate, date_death) VALUES ('90f0b889-6d41-448e-ada4-de0d1285b173', 'MARIA ENRIQUEZ', 'F','Argentina', '1973-12-06', '');
INSERT INTO authors (id, name, gender, country_origin, birthdate, date_death) VALUES ('17c53acf-9678-4b9d-90bc-c93c5652eeab', 'JULIO VERNE', 'M','Francia', '1828-02-08', '1905-03-24');
INSERT INTO authors (id, name, gender, country_origin, birthdate, date_death) VALUES ('76a98975-3cac-4ec0-9e00-558fc4ee5994', 'HOMERO', 'M','Grecia', '1828-02-08', '1905-03-24');

INSERT INTO books (id, title, publication_year, author_id) VALUES ('701a197a-4589-4b4c-afe6-d5d0b633d026', 'Don Quijote de la Mancha', 1605, 'd1000b02-286c-4bb5-a7ec-f5d3478e8215');
INSERT INTO books (id, title, publication_year, author_id) VALUES ('83060a51-b481-48c0-aea6-a24e95b37143', 'Harry Potter y el prisionero de Azkaban', 1999, '7d88581b-5602-4bf9-9bfd-4bcf11bd1e9a');
INSERT INTO books (id, title, publication_year, author_id) VALUES ('e2970609-2b6b-4d27-a55d-39a6d6dcedb8', 'Harry Potter y la cámara secreta', 1998, '7d88581b-5602-4bf9-9bfd-4bcf11bd1e9a');
INSERT INTO books (id, title, publication_year, author_id) VALUES ('71fa9de2-a814-4490-a331-331b552c83c1', 'Cien años de soledad', 1967, '8db69427-7a1e-40e9-8de5-e4b437a0b6b1');
INSERT INTO books (id, title, publication_year, author_id) VALUES ('eaf23672-cd14-410f-9def-caf7973a9327', 'La vuelta al mundo en ochenta días', 1872, '17c53acf-9678-4b9d-90bc-c93c5652eeab');
INSERT INTO books (id, title, publication_year, author_id) VALUES ('4b4a36e0-1ae8-4b68-8931-fb428da4cbc4', 'Crónica de una muerte anunciada', 1981, '8db69427-7a1e-40e9-8de5-e4b437a0b6b1');
INSERT INTO books (id, title, publication_year, author_id) VALUES ('bcfcdc1f-74e9-4f74-8a68-a487b2753da4', 'La Gitanilla', 1613, 'd1000b02-286c-4bb5-a7ec-f5d3478e8215');
INSERT INTO books (id, title, publication_year, author_id) VALUES ('bd8f879b-fd74-47f2-b6eb-dc789b6fe3a8', 'ODISEA', 1600, '76a98975-3cac-4ec0-9e00-558fc4ee5994');
INSERT INTO books (id, title, publication_year, author_id) VALUES ('0e0060e8-edd3-4998-bbc0-89e56eb61cc5', 'ILIADA', 1600, '76a98975-3cac-4ec0-9e00-558fc4ee5994');
INSERT INTO books (id, title, publication_year, author_id) VALUES ('129dfb5e-1e01-48c9-b795-275c2dc60098', 'Cinco semanas en globo', 1863, '17c53acf-9678-4b9d-90bc-c93c5652eeab');
INSERT INTO books (id, title, publication_year, author_id) VALUES ('8dddb782-7842-4fa2-9a54-5d497c97c803', 'Relato de un náufrago', 1955, '8db69427-7a1e-40e9-8de5-e4b437a0b6b1');