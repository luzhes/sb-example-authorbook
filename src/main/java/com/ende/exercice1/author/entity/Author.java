package com.ende.exercice1.author.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "authors")
public class Author {

  @Id
  @GeneratedValue
  private UUID id;
  private String name;
  private String gender;
  @Column(name = "country_origin")
  private String countryOrigin;
  private String birthdate;
  @Column(name = "date_death")
  private String dateDeath;
}
