package com.ende.exercice1.author.service;

import com.ende.exercice1.author.dto.AuthorDto;
import com.ende.exercice1.author.entity.Author;
import com.ende.exercice1.author.exception.EntityNotFoundException;
import com.ende.exercice1.author.repository.AuthorRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthorServiceImplement implements AuthorService {

  private AuthorRepository authorRepository;

  @Override
  public Author getById(UUID id) {
    return authorRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("Author", id));
  }

  @Override
  public List<Author> getAll() {
    return authorRepository.findAll();
  }

  @Override
  public Author create(AuthorDto dto) {
    Author author = new Author();
    author.setName(dto.getName());
    author.setGender(dto.getGender());
    author.setCountryOrigin(dto.getCountryOrigin());
    author.setBirthdate(dto.getBirthdate());
    author.setDateDeath(dto.getDateDeath());
    return authorRepository.save(author);
  }

  @Override
  public Author updateAuthor(UUID id, AuthorDto dto) {
    Author authorFound = authorRepository.findById(id).get();
    authorFound.setName(dto.getName());
    authorFound.setGender(dto.getGender());
    authorFound.setCountryOrigin(dto.getCountryOrigin());
    authorFound.setBirthdate(dto.getBirthdate());
    authorFound.setDateDeath(dto.getDateDeath());
    return authorRepository.save(authorFound);
  }

  @Override
  public Author getByBirthdate(String birthdate) {
    return authorRepository.findByBirthdate(birthdate)
        .orElseThrow(() -> new EntityNotFoundException("Author not found"));
  }

  @Override
  public Author patchData(UUID id, Author author) {
    Optional<Author> optional = authorRepository.findById(id);
    if (optional.isPresent()) {
      Author existingAuthor = optional.get();
      if (author.getName() != null) {
        existingAuthor.setName(author.getName());
      }
      if (author.getGender() != null) {
        existingAuthor.setGender(author.getGender());
      }
      if (author.getCountryOrigin() != null) {
        existingAuthor.setCountryOrigin(author.getCountryOrigin());
      }
      if (author.getBirthdate() != null) {
        existingAuthor.setBirthdate(author.getBirthdate());
      }
      if (author.getDateDeath() != null) {
        existingAuthor.setDateDeath(author.getDateDeath());
      }
      return authorRepository.save(existingAuthor);
    } else {
      return null;
    }
  }

  @Override
  public String deleteById(UUID id) {
    authorRepository.deleteById(id);
    return "Author deleted successfully";
  }

  @Override
  public Author getByName(String name) {
    return authorRepository.findByName(name)
        .orElseThrow(() -> new EntityNotFoundException("Author not found"));
  }
}
