package com.ende.exercice1.author.service;

import com.ende.exercice1.author.dto.BookDto;
import com.ende.exercice1.author.entity.Author;
import com.ende.exercice1.author.entity.Book;
import com.ende.exercice1.author.exception.EntityNotFoundException;
import com.ende.exercice1.author.mapper.BookMapper;
import com.ende.exercice1.author.repository.BookRepository;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class BookServiceImplement implements BookService {

  private BookRepository bookRepository;
  private AuthorService authorService;
  private BookMapper bookMapper;

  @Override
  public Book getById(UUID id) {
    Book book = bookRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("Book", id));
    return book;
  }

  @Override
  public List<Book> getAll() {
    return bookRepository.findAll();
  }

  @Override
  public Book create(BookDto dto) {
    Author author = authorService.getById(dto.getAuthorId());
    Book book = bookMapper.fromDto(dto);
    book.setAuthor(author);
    return bookRepository.save(book);
  }

  @Override
  public Book update(UUID id, BookDto dto) {
    Book bookFound = bookRepository.findById(id).orElse(null);
    if (bookFound != null) {
      if (dto.getTitle() != null) {
        bookFound.setTitle(dto.getTitle());
      }
      if (dto.getPublicationYear() != null) {
        bookFound.setPublicationYear(dto.getPublicationYear());
      }
      if (dto.getAuthorId() != null) {
        bookFound.setAuthor(authorService.getById(dto.getAuthorId()));
      }
      return bookRepository.save(bookFound);
    } else {
      return null;
    }
  }

  @Override
  public String deleteById(UUID id) {
    bookRepository.deleteById(id);
    return "Book deleted successfully";
  }

  @Override
  public Book getByTitle(String title) {
    return bookRepository.findByTitle(title)
        .orElseThrow(() -> new EntityNotFoundException("Book not found"));
  }

  @Override
  public List<Book> getByPublicationYear(Integer publicationYear) {
    return bookRepository.findByPublicationYear(publicationYear)
        .orElseThrow(() -> new EntityNotFoundException("Book not found"));
  }
}
