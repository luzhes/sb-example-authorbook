package com.ende.exercice1.author.service;

import com.ende.exercice1.author.dto.AuthorDto;
import com.ende.exercice1.author.dto.BookDto;
import com.ende.exercice1.author.entity.Author;
import com.ende.exercice1.author.entity.Book;
import java.util.List;
import java.util.UUID;

public interface BookService {

  Book getById(UUID id);

  List<Book> getAll();

  Book create(BookDto bookDto);

  String deleteById(UUID id);

  Book getByTitle(String name);

  List<Book> getByPublicationYear(Integer publicationYear);

  Book update(UUID id, BookDto dto);
}
