package com.ende.exercice1.author.service;

import com.ende.exercice1.author.dto.AuthorDto;
import com.ende.exercice1.author.entity.Author;
import java.util.List;
import java.util.UUID;

public interface AuthorService {
  Author getById(UUID id);

  List<Author> getAll();

  Author create(AuthorDto authorDto);

  String deleteById(UUID id);

  Author getByName(String name);

  Author updateAuthor(UUID id, AuthorDto author);

  Author getByBirthdate(String birthdate);

  Author patchData(UUID id, Author author);
}
