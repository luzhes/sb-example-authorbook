package com.ende.exercice1.author.repository;

import com.ende.exercice1.author.entity.Author;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author, UUID> {

  Optional<Author> findByName(String name);
  Optional<Author> findByBirthdate(String date);
}
