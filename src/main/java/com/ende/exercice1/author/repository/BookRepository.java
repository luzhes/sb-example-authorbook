package com.ende.exercice1.author.repository;

import com.ende.exercice1.author.entity.Book;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<Book, UUID> {

  Optional<Book> findByTitle(String name);

  Optional<List<Book>> findByPublicationYear(Integer publicationYear);
}
