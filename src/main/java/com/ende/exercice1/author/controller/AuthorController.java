package com.ende.exercice1.author.controller;


import com.ende.exercice1.author.dto.AuthorDto;
import com.ende.exercice1.author.entity.Author;
import com.ende.exercice1.author.service.AuthorService;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/authors")
public class AuthorController {

  private AuthorService authorService;

  @GetMapping("/{id}")
  public ResponseEntity<Author> getById(@PathVariable UUID id) {
    Author authorFound = authorService.getById(id);
    return ResponseEntity.status(HttpStatus.OK).body(authorFound);
  }

  @GetMapping
  public ResponseEntity<List<Author>> getAll() {
    List<Author> authors = authorService.getAll();
    return ResponseEntity.status(HttpStatus.OK).body(authors);
  }

  @PostMapping
  public ResponseEntity<Author> create(@RequestBody AuthorDto dto) {
    Author authorSaved = authorService.create(dto);
    return ResponseEntity.status(HttpStatus.CREATED).body(authorSaved);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Author> update(@PathVariable UUID id, @RequestBody AuthorDto dto) {
    Author updatedAuthor = authorService.updateAuthor(id, dto);
    return ResponseEntity.status(HttpStatus.OK).body(updatedAuthor);
  }

  @DeleteMapping("/{id}")
  public String delete(@PathVariable UUID id) {
    return authorService.deleteById(id);
  }

  @GetMapping("name/{name}")
  public ResponseEntity<Author> getByName(@PathVariable String name) {
    Author authorFound = authorService.getByName(name);
    return ResponseEntity.status(HttpStatus.OK).body(authorFound);
  }

  @GetMapping("birthdate/{birthdate}")
  public ResponseEntity<Author> getByBirthdate(@PathVariable String birthdate) {
    Author authorFound = authorService.getByBirthdate(birthdate);
    return ResponseEntity.status(HttpStatus.OK).body(authorFound);
  }
  @PatchMapping("/{id}")
  public ResponseEntity<Author> patchAuthor(@PathVariable UUID id, @RequestBody Author author) {
    Author authorUpdate = authorService.patchData(id, author);
    if (authorUpdate != null) {
      return new ResponseEntity<>(author, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }
}
