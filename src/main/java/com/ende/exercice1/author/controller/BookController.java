package com.ende.exercice1.author.controller;

import com.ende.exercice1.author.dto.BookDto;
import com.ende.exercice1.author.entity.Book;
import com.ende.exercice1.author.service.BookService;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/books")
public class BookController {

  private BookService bookService;

  @GetMapping("/{id}")
  public ResponseEntity<Book> getById(@PathVariable UUID id) {
    Book bookFound = bookService.getById(id);
    return ResponseEntity.status(HttpStatus.OK).body(bookFound);
  }

  @GetMapping
  public ResponseEntity<List<Book>> getAll() {
    List<Book> books = bookService.getAll();
    return ResponseEntity.status(HttpStatus.OK).body(books);
  }

  @PostMapping
  public ResponseEntity<Book> create(@RequestBody BookDto dto) {
    Book bookSaved = bookService.create(dto);
    return ResponseEntity.status(HttpStatus.CREATED).body(bookSaved);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Book> updateBook(@PathVariable UUID id, @RequestBody BookDto dto) {
    Book bookUpdated = bookService.update(id, dto);
    if (bookUpdated != null) {
      return ResponseEntity.status(HttpStatus.OK).body(bookUpdated);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/{id}")
  public String delete(@PathVariable UUID id) {
    return bookService.deleteById(id);
  }

  @GetMapping("title/{title}")
  public ResponseEntity<Book> getByName(@PathVariable String title) {
    Book bookFound = bookService.getByTitle(title);
    return ResponseEntity.status(HttpStatus.OK).body(bookFound);
  }

  @GetMapping("publicationYear/{year}")
  public List<Book> getByYear(@PathVariable Integer year) {
    return bookService.getByPublicationYear(year);
  }
}

