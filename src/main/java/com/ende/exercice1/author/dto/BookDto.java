package com.ende.exercice1.author.dto;

import jakarta.persistence.Entity;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class BookDto {

  private String title;
  private Integer publicationYear;
  private UUID authorId;

}
