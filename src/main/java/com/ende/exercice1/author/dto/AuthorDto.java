package com.ende.exercice1.author.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class AuthorDto {

  private String name;
  private String gender;
  private String countryOrigin;
  private String birthdate;
  private String dateDeath;
}
