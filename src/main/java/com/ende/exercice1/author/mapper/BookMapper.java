package com.ende.exercice1.author.mapper;

import com.ende.exercice1.author.dto.BookDto;
import com.ende.exercice1.author.entity.Book;
import org.springframework.stereotype.Component;

@Component
public class BookMapper {

  public Book fromDto(BookDto bookDto) {
    Book book = new Book();
    book.setTitle(bookDto.getTitle());
    book.setPublicationYear(bookDto.getPublicationYear());
    return book;
  }

}
